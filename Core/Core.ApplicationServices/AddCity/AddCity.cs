﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class AddCity : IAddCity
    {
        private readonly IUnitOfWork unit;

        public AddCity(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(AddCityDto dto)
        {
            ApiResult result = new ApiResult { Status = true};
            var now = DateTime.Now.ToUnix();
            City city = new City
            {
                Id = unit.City.GetMax<int>(p => p.Id) + 1,
                Name = dto.Name,
                ProvinceId = dto.ProvienceId
            };
            unit.City.Add(city);
            unit.Complete();
            result.Response = city;
            return result;
        }
    }
}
