﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class AddMoneyRequest : IAddMoneyRequest
    {
        private readonly IUnitOfWork unit;

        public AddMoneyRequest(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public BaseApiResult Execute(AddMoneyRequestDto dto)
        {
            BaseApiResult result = new BaseApiResult { Status = true,Message = Messages.Success};
            var now = DateTime.Now.ToUnix();
            User user = unit.Users.Get(dto.UserId);
            if (user.Sheba == null || user.Sheba == "")
            {
                result.Status = false;
                result.Message = Messages.ShebaNotExist;
            }
            else
            {
                MoneyRequest mr = new MoneyRequest
                {
                    Amount = dto.Amount,
                    CreatedAt = now,
                    Description = dto.Description,
                    UserId = dto.UserId,
                    Status = MoneyRequestCode.Pending.ToInt(),
                                       
                };
                unit.MoneyRequests.Add(mr);
                unit.Complete();
            }
            return result;
        }
    }
}
