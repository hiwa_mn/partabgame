﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class AddProvience : IAddProvience
    {
        private readonly IUnitOfWork unit;

        public AddProvience(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(NameDto dto)
        {
            ApiResult result = new ApiResult { Status = true};
            var now = DateTime.Now.ToUnix();
            Province pr = new Province
            {
                Id = unit.City.GetMax<int>(p => p.Id) + 1,
                Name = dto.Name,
                CountryId = 1
            };
            unit.Province.Add(pr);
            unit.Complete();
            result.Response = pr;
            return result;
        }
    }
}
