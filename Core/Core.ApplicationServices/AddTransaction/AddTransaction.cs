﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class AddTransaction : IAddTransaction
    {
        private readonly IUnitOfWork unit;

        public AddTransaction(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public BaseApiResult Execute(AddTransactionDto dto)
        {
            BaseApiResult result = new BaseApiResult { Status = true,Message = Messages.Success};
            var now = DateTime.Now.ToUnix();
            Transaction trans = new Transaction
            {
                Amount = dto.Amount,
                CreatedAt = now,
                IsSuccessful = true,
                Description = "برد یا باخت یا ورود به روم",
                UserId = dto.UserId,
                TransactionCategoryId = dto.TransactionCategoryId
            };
            unit.Transaction.Add(trans);
            unit.Complete();            
            return result;
        }
    }
}
