﻿using Core.Contracts;
using Core.Entities.Dto;
using Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class ChangeMoneyResuestStatus : IChangeMoneyResuestStatus
    {
        private readonly IUnitOfWork unit;

        public ChangeMoneyResuestStatus(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public BaseApiResult Execute(ChangeMoneyResuestStatusDto dto)
        {
            BaseApiResult result = new BaseApiResult { Status = true };

            var mr = unit.MoneyRequests.Get(dto.Id);

            mr.Status = dto.Status;
            mr.Description = dto.Description;
            if (dto.Status == MoneyRequestCode.Accepted.ToInt())
            {
                mr.ChangeStatusTime = Agent.Now;
                unit.Transaction.Add(new Entities.Transaction
                {
                    Amount = -mr.Amount,
                    CreatedAt = Agent.Now,
                    IsSuccessful = true,
                    Description = dto.Description,
                    UserId = mr.UserId,
                    TransactionCategoryId = TransactionType.Released.ToInt()
                });
            }
                
                
            unit.Complete();
            return result;
        }
    }
}
