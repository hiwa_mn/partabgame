﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utility;
using Utility.Tools.General;
using Utility.Tools.Notification;

namespace Core.ApplicationServices
{
    public class ChangeProfilePicture : IChangeProfilePicture
    {
        private readonly IUnitOfWork unit;


        public ChangeProfilePicture(IUnitOfWork unit)
        {
            this.unit = unit;

        }

        public BaseApiResult Execute(ChangeProfilePictureDto dto)
        {
            BaseApiResult Result = new BaseApiResult { Message = Messages.OK, Status = true };
            var user = unit.Users.Get(dto.UserId);            
            user.ProfileImageId = dto.ProfileImageId;
            
            unit.Complete();
            return Result;
        }
    }
}
