﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utility;
using Utility.Tools.Auth;
using Utility.Tools.General;
using Utility.Tools.Notification;

namespace Core.ApplicationServices
{
    public class CheckActiveCode : ICheckActiveCode
    {
        private readonly IUnitOfWork unit;
        private readonly IJwtHandler jwtHandler;

        public CheckActiveCode(IUnitOfWork unit,
            IJwtHandler jwtHandler
            )
        {
            this.unit = unit;
            this.jwtHandler = jwtHandler;
        }

        public CheckActiveCodeResultDto Execute(CheckActiveCodeDto dto)
        {
            var now = Agent.UnixTimeNow();
            var Result = new CheckActiveCodeResultDto { Message = Messages.NotOK,Object = new UserDto { } };
            if (unit.ActiveCode.CheckActiveCode(dto))
            {
                Result.Message = Messages.OK;
                Result.Status = true;
                var user = unit.Users.GetByMobile(dto.Mobile);
                if (user != null)
                {
                    Result.Object= DtoBuilder.CreateUserDto(user);
                    Result.Object.Token = jwtHandler.Create(user.Id);                    
                }
                else
                {
                    User newUser = new User()
                    {
                        Mobile = dto.Mobile,
                        CreatedAt = now,
                        Device = new List<Device> { new Device { PushId = dto.PushId, CreatedAt = now } },
                        Status = Enums.EntityStates.Active.ToInt(),
                    };
                    unit.Users.Add(newUser);
                    unit.Complete();
                    Result.Object = DtoBuilder.CreateUserDto(newUser);
                    Result.Object.Token = jwtHandler.Create(newUser.Id);
                }
                if (!unit.Device.IsExist(Result.Object.Id, dto.PushId))
                {
                    unit.Device.Add(new Device { PushId = dto.PushId, CreatedAt = now, UserId = Result.Object.Id });
                    unit.Complete();
                }
            }
            return Result;
        }
    }
}
