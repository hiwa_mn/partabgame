﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class DeleteCity : IDeleteCity
    {
        private readonly IUnitOfWork unit;

        public DeleteCity(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(BaseByIntIdDto dto)
        {
            ApiResult result = new ApiResult { Status = false, Message = Messages.IsUsed };

            try
            {
                City city = unit.City.Get(dto.Id);
                unit.City.Remove(city);
                unit.Complete();
                result.Status = true;

            }
            catch
            {
            }
            return result;
        }
    }
}
