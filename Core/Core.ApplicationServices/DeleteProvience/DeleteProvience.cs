﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class DeleteProvience : IDeleteProvience
    {
        private readonly IUnitOfWork unit;

        public DeleteProvience(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(BaseByIntIdDto dto)
        {
            ApiResult result = new ApiResult { Status = false, Message = Messages.IsUsed };

            try
            {
                Province pr = unit.Province.Get(dto.Id);
                unit.Province.Remove(pr);
                unit.Complete();
                result.Status = true;

            }
            catch
            {
            }
            return result;
        }
    }
}
