﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class GetCities : IGetCities
    {
        private readonly IUnitOfWork unit;

        public GetCities(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public GetCitiesResultDto Execute(GetCitiesDto dto)
        {
            Province pr = unit.Province.GetIncludeCtiesById(dto);
            return new GetCitiesResultDto
            {
                Status = true,
                Object = new ProvinceResponse
                {
                    Cities = pr.City.Select(p => new CityResponse
                    {
                        City = new FixedIntDto { Id = p.Id, Name = p.Name },
                        UserCount = p.User.Count()
                    }).ToList(),
                    Province = new FixedIntDto { Id = pr.Id, Name = pr.Name },
                    UserCount = pr.City.Sum(q => q.User.Count)

                }
            };
        }
    }
}
