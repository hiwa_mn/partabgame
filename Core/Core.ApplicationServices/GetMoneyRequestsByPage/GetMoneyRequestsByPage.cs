﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class GetMoneyRequestsByPage : IGetMoneyRequestsByPage
    {
        private readonly IUnitOfWork unit;

        public GetMoneyRequestsByPage(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public GetMoneyRequestsByPageResultDto Execute(GetMoneyRequestsByPageDto dto)
        {
            return unit.MoneyRequests.GetMoneyRequestsByPage(dto);            
        }
    }

    
}
