﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class GetMoneyRequetsSum : IGetMoneyRequetsSum
    {
        private readonly IUnitOfWork unit;

        public GetMoneyRequetsSum(IUnitOfWork unit)
        {
            this.unit = unit;
        }

   

        public MoneyRequestSumDto Execute(GetMoneyRequestSumDto dto)
        {
            return unit.MoneyRequests.GetMonyRequestSum(dto);
        }
    }

    
}
