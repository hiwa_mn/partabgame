﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class GetProviences : IGetProviences
    {
        private readonly IUnitOfWork unit;

        public GetProviences(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public GetProvincesResultDto Execute()
        {
            var prs = unit.Province.GetIncludeCties();
            return new GetProvincesResultDto
            {
                Status = true,
                Object = prs.Select(p=>new ProvinceResponse
                {
                    Cities = new List<CityResponse> { },
                    Province = new FixedIntDto { Id = p.Id,Name = p.Name},                                        
                    UserCount = p.Cities.Sum(q=>q.UserCount)
                }).ToList()
            };
        }
    }
}
