﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class GetRooms : IGetRooms
    {
        private readonly IUnitOfWork unit;

        public GetRooms(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public GetRoomsResultDto Execute()
        {
            
            return new GetRoomsResultDto
            {
                Status = true,
                Object = unit.Rooms.GetAll().Select(p=>DtoBuilder.CreateRoomDto(p)).ToList()
            };
        }
    }
}
