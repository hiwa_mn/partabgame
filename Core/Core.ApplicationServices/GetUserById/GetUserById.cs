﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using Core.Entities.GlobalSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;
using Utility.Tools.Auth;
using Utility.Tools.General;
using Utility.Tools.Notification;

namespace Core.ApplicationServices
{
    public class GetUserById : IGetUserById
    {
        private readonly IUnitOfWork unit;
        private readonly IJwtHandler jwtHandler;

        public GetUserById(IUnitOfWork unit,
            IJwtHandler jwtHandler
            )
        {
            this.unit = unit;
            this.jwtHandler = jwtHandler;
        }

        public CheckActiveCodeResultDto Execute(BaseByUserDto dto)
        {
            var now = Agent.UnixTimeNow();
            var Result = new CheckActiveCodeResultDto { Message = Messages.NotOK };

            //var user = unit.Users.GetById(dto.UserId.Value);

            //UserInfo userDto = DtoBuilder.CreateUserInfoDto(user);
            //userDto.Token = jwtHandler.Create(user.Id);

            //Result.Object = getSplashObject.Execute(userDto);


            return Result;
        }
    }
}
