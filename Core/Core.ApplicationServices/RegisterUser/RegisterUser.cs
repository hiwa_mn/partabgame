﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.Auth;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class RegisterUser : IRegisterUser
    {
        private readonly IUnitOfWork unit;
        private readonly IEncrypter encrypter;
        private readonly IJwtHandler jwtHandler;

        public RegisterUser(IUnitOfWork unit, IEncrypter encrypter,IJwtHandler jwtHandler)
        {
            this.unit = unit;
            this.encrypter = encrypter;
            this.jwtHandler = jwtHandler;
        }
        public LoginResultDto Execute(RegisterUserDto dto)
        {   
            var now = Agent.UnixTimeNow();
            //var kar = unit.Kargozari.Get(dto.KargozariId);
            //User user = new User()
            //{
            //    FamilyName = dto.FamilyName,
            //    Name = dto.Name,
            //    Mobile = dto.Mobile,
            //    CreatedAt = now,
            //    Status = Enums.EntityStates.Active.ToInt(),
            //    KargozariId = dto.KargozariId,
            //    UserRole = new List<UserRole> { new UserRole {CreatedAt = now,RoleId = dto.RoleId } },
            //    Gender = dto.Gender,
            //    CityId = kar?.CityId
            //};
            //Password pass = new Password
            //{
            //    CreatedAt = now,
            //    PasswordTypeId = PasswordTypes.Login.ToInt(),

            //};
            //pass.SetPassword(dto.Password,encrypter);
            //user.Passwords = new List<Password> { pass};            
            //unit.Users.Add(user);
            //unit.Complete();
            //var role = unit.Roles.Get(dto.RoleId);
            //user.UserRole.FirstOrDefault().Role = role;
            var result = new LoginResultDto
            {
                //Object = DtoBuilder.CreateUserDto(user),
                Status = true
            };
            //result.Object.Token = jwtHandler.Create(user.Id);
            return result;
        }
    }
}
