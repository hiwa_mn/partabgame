﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.GlobalSettings;
using Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utility;
using Utility.Tools.General;
using Utility.Tools.Notification;

namespace Core.ApplicationServices
{
    public class SendActiveCode : ISendActiveCode
    {
        private readonly IUnitOfWork unit;
        private readonly INotification notification;

        public SendActiveCode(IUnitOfWork unit, INotification notification)
        {
            this.unit = unit;
            this.notification = notification;
        }

        public BaseApiResult Execute(SendActiveCodeDto dto)
        {
            BaseApiResult result = new BaseApiResult { Message = Messages.LimitExceeded };
            var now = Agent.UnixTimeNow();
            if (!unit.ActiveCode.CheckExeed(dto.Mobile))
            {
                string Code = Agent.GenerateRandomNo(6);
                unit.ActiveCode.Add(new ActiveCode() { Mobile = dto.Mobile, Code = Code, CreatedAt = now });
                unit.Complete();
                var sent = $"{AdminSettings.SMSName}\n{AdminSettings.SMSTitle}:{Code}";
                Task myTask = Task.Run(() => notification.SendAsync(sent, dto.Mobile));
                myTask.Wait();
                result.Message = Messages.OK;
                result.Status = true;
            }
            return result;
        }
    }
}
