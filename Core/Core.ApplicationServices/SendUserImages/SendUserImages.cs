﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class SendUserImages : ISendUserImages
    {
        private readonly IUnitOfWork unit;

        public SendUserImages(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(SendUserImagesDto dto)
        {            
            ApiResult result = new ApiResult { Status = true};
            var now = DateTime.Now.ToUnix();
            var user = unit.Users.Get(dto.UserId);
            user.ProfileImageId = dto.ProfileImageId;
            if (dto.Images != null && dto.Images.Count != 0)
            {
                List<UserImages> ui = dto.Images.Select(p => new UserImages
                {
                    CreatedAt = now,
                    DocumentId = p.ImageId,
                    ImageStatusId = ImageStatuses.Pending.ToInt(),
                    ImageTypeId = p.ImageTypeId,
                    UserId = dto.UserId.Value
                }).ToList();
                unit.UserImages.AddRange(ui);
            }
            unit.Complete();
            return result;
        }
    }
}
