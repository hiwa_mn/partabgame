﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class UpdateCity : IUpdateCity
    {
        private readonly IUnitOfWork unit;

        public UpdateCity(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(FixedIntDto dto)
        {
            ApiResult result = new ApiResult { Status = true };

            City city = unit.City.Get(dto.Id);
            city.Name = dto.Name;
            unit.Complete();
            result.Response = city;
            return result;
        }
    }
}
