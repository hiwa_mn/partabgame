﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace Core.ApplicationServices
{
    public class UpdateProvience : IUpdateProvience
    {
        private readonly IUnitOfWork unit;

        public UpdateProvience(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public ApiResult Execute(FixedIntDto dto)
        {
            ApiResult result = new ApiResult { Status = true };

            Province province = unit.Province.Get(dto.Id);
            province.Name = dto.Name;
            unit.Complete();
            result.Response = province;
            return result;
        }
    }
}
