﻿using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Contracts
{
    public interface IMoneyRequestRepository : IRepository<MoneyRequest>
    {
        GetMoneyRequestsByPageResultDto GetMoneyRequestsByPage(GetMoneyRequestsByPageDto dto);
        MoneyRequestSumDto GetMonyRequestSum(GetMoneyRequestSumDto dto);
    }
}
