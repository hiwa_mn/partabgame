﻿using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Contracts
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        bool IsAuthorityExist(string authority);
        Transaction GetByAuthority(string authority);
        int GetWallet(BaseByUserDto dto);
        List<TransactionDto> GetTransactionsByPage(GetTransactionsByPageDto dto);
        int GetTransactionsByPageCount(GetTransactionsByPageDto dto);
    }
}
