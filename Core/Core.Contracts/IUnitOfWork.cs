﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Contracts
{
    public interface IUnitOfWork
    {
        ICityRepository City { get; set; }
        ICountryRepository Country { get; set; }
        IProvinceRepository Province { get; set; }
        IDocumentRepository Document { get; set; }
        IActiveCodeRepository ActiveCode { get; set; }
        IDeviceRepository Device { get; set; }
        INotificationRepository Notification { get; set; }
        ISettingRepository Setting { get; set; }
        IUpdateRepository Update { get; set; }
        IRoleRepository Roles { get; set; }
        IUserRepository Users { get; set; }
        IUserRoleRepository UserRoles { get; set; }
        ISliderRepository Slider{ get; set; }
        IImageStatusRepository ImageStatus{ get; set; }
        IImageTypeRepository ImageType{ get; set; }
        ITransactionCategoryRepository TransactionCategory{ get; set; }
        ITransactionRepository Transaction{ get; set; }
        IUserImagesRepository UserImages{ get; set; }
        IRoomRepository Rooms { get; set; }
        IMoneyRequestRepository MoneyRequests { get; set; }
       

        void Complete();
    }
}
