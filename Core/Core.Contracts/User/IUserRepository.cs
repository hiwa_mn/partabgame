﻿using Core.Entities;
using Core.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByMobile(string mobile);
        User GetByEmail(string Email);

        User GetByEmailIncludingRoles(string email);
        User GetByMobileIncludingRoles(string mobile);

        List<UserDto> GetUsersByPage(GetUsersByPageDto dto);

        bool IsExist(string Email);
        int GetUsersByPageCount(GetUsersByPageDto dto);
        User GetById(Guid userId);
        List<User> GetAllByDetails();
    }
}
