﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AddCityDto
    {
        public string Name { get; set; }
        public int ProvienceId{ get; set; }
    }
}
