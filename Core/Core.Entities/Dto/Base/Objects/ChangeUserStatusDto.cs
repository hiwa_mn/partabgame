﻿using Enums;
using System;

namespace Core.Entities.Dto
{
    public class ChangeUserStatusDto
    {
        public Guid UserId { get; set; }
        public Enums.EntityStates UserStatus { get; set; }

    }
}
