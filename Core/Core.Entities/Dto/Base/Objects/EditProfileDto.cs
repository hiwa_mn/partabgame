﻿using System;

namespace Core.Entities.Dto
{
    public class EditProfileDto
    {
        public Guid UserId { get; set; }
        public string Name{ get; set; }
        public string FamilyName{ get; set; }
        public long? BirthDay { get; set; }
        public string NationalCode { get; set; }
        public int? Gender { get; set; }
        public string Email { get; set; }
        public string Sheba { get; set; }
    }
}
