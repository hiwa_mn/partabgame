﻿
using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetCitiesResultDto : BaseApiResult
    {
        public ProvinceResponse Object { get; set; }

    }
    public class GetProvincesResultDto : BaseApiResult
    {
        public List<ProvinceResponse> Object { get; set; }

    }
    public class ProvinceResponse
    {
        public List<CityResponse> Cities { get; set; }
        public FixedIntDto Province { get; set; }
        public int UserCount { get; set; }
    }
    public class CityResponse
    {
        public FixedIntDto City { get; set; }

        public int UserCount { get; set; }

    }


}
