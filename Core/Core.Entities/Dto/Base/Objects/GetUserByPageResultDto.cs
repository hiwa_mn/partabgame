﻿
using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetUserByPageResultDto : BaseApiPageResult
    {
        public List<UserDto> Object { get; set; }
    }


}
