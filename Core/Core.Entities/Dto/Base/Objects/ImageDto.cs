﻿
using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class ImageDto
    {
        public string Address{ get; set; }
        public Guid Id { get;  set; }        
    }
}
