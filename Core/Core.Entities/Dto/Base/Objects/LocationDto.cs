﻿
using System;
using System.Collections.Generic;
using Utility.Tools.Auth;

namespace Core.Entities.Dto
{
    public class LocationDto
    {
        public int? CityId { get; set; }
        public int?  ProvienceId { get; set; }
        public int? CountryId { get; set; }
        public string CityName { get; set; }
        public string ProvienceName { get; set; }
        public string CountryName { get; set; }
        public double? Lat{ get; set; }
        public double? Lon{ get; set; }
    }
}
