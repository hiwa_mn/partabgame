﻿
using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class ProvinceDto
    {
        public string Name { get; set; }
        public int Id { get;  set; }
        public int UserCount { get;  set; }
        public List<CityResponse> Cities { get; set; }
    }
}
