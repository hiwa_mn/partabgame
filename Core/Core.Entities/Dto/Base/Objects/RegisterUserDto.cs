﻿using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities.Dto
{
    public class RegisterUserDto
    {        
        public string Name { get; set; }
        public string FamilyName { get; set; }       
        public string Mobile { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public int? Gender { get; set; }
    }
}
