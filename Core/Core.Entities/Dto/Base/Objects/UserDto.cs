﻿
using System;
using System.Collections.Generic;
using Utility.Tools.Auth;

namespace Core.Entities.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; internal set; }
        public long? Birthday { get; internal set; }
        public string Mobile { get; set; }
        public long CreatedAt { get; internal set; }
        public int? Gender { get; internal set; }
        public JsonWebToken Token { get; set; }        
        public ImageDto ProfileImage{ get; internal set; }
        public LocationDto Location { get; internal set; }
        public string Email { get; internal set; }
        public int Status { get; internal set; }
        public string Sheba { get; internal set; }
    }
    public class UserShortDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Sheba { get; set; }

        public ImageDto ProfileImage { get; set; }

        
    }
}
