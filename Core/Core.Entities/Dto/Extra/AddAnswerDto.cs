﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AddAnswerDto
    {
        public string Json { get; set; }
        public Guid ProcessId { get; set; }
        public int ProcessStatusId { get; set; }
        public bool IsForAdmin { get; set; }
        public Guid? UserId { get; set; }
    }
}
