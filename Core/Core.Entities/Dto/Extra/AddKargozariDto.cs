﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AddKargozariDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public string Merchant { get; set; }
        public string Description { get; set; }
        public double? Lon { get; set; }
        public double? Lat { get; set; }
        public string Address { get; set; }
    }
}
