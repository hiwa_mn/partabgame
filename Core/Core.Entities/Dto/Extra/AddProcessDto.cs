﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AddProcessDto
    {
        public int ProcessTypeId { get; set; }
        public string Json { get; set; }        
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public int? CityId { get; set; }
        public int? DeliveryTypeId { get; set; }   
        public List<Guid> Images { get; set; }
        public int? KargozariId { get; set; }
        public Guid UserId { get; set; }
    }
}
