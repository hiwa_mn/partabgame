﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AddTransactionDto
    {                          
        public int Amount { get; set; }
        public Guid UserId { get; set; }
        public int TransactionCategoryId { get; set; }
    }
}
