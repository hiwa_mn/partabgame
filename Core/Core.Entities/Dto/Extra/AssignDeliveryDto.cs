﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AssignDeliveryDto
    {
        public Guid ProcessId { get; set; }
        public Guid DeliveryId { get; set; }
        public int ProcessStatusId { get; set; }
    }
}
