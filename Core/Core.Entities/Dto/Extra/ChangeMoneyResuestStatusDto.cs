﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class ChangeMoneyResuestStatusDto
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public string Description{ get; set; }
    }
}
