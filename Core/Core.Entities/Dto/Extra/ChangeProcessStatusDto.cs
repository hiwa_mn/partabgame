﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class ChangeProcessStatusDto
    {
        public Guid ProcessId { get; set; }
        public int ProcessStatusId { get; set; }
        public string Reason{ get; set; }
    }
}
