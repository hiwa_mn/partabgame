﻿using System;

namespace Core.Entities.Dto
{
    public class ChangeProfilePictureDto
    {
        public Guid UserId { get; set; }        
        public Guid? ProfileImageId { get; set; }
    }
}
