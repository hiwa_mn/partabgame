﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class EditProcessTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string ResponsePattern { get; set; }
        public string ErrorPattern { get; set; }
        public List<string> Errors{ get; set; }
    }
}
