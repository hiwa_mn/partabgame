﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class FollowAProcessDto
    {
        public Guid ProcessId { get; set; }
        public Guid OperatorId { get; set; }
        public int ProcessStatusId { get; set; }
    }
}
