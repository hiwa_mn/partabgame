﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetDeliveryDetailDto 
    {
        public Guid ProcessId { get; set; }
    }
    public class GetDeliveryDetailResultDto : BaseApiResult
    {
        public DeliveryInfo Object { get; set; }
    }

    public class DeliveryInfo
    {
        public Location Delivery { get; set; }
        public Location Kargozari { get; set; }
        public Location Destination { get; set; }
    }
    public class Location
    {
        public double? Lat { get; set; }
        public double? Lon { get; set; }
    }
}
