﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetKargozariByIdDto : BaseByIntIdDto
    {
    }
    public class GetKargozariByIdResultDto : BaseApiResult
    {
        public KargozariDetailDto Object { get; set; }
    }

}
