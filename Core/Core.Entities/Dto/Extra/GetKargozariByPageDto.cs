﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetKargozariByPageDto : BaseByPageDto
    {
    }
    public class GetKargozariByPageResultDto : BaseApiPageResult
    {
        public List<KargozariDetailDto> Object { get; set; }
    }

}
