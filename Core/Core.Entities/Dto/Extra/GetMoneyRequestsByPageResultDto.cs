﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetMoneyRequestsByPageDto : BaseByUserPageDto
    {
        public int Status{ get; set; }
    }
    public class GetMoneyRequestsByPageResultDto : BaseApiPageResult
    {
        public List<MoneyRequestDto> Object { get; set; }
    }
}
