﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetProcessByPageDto : BaseByUserPageDto
    {
        public string NationalCode { get; set; }
        public string Mobile { get; set; }
        public string BimCode { get; set; }
        public string KargahCode { get; set; }
        public int KargozariId { get; set; }
        public int ProcessStatusId { get; set; }
        public int CityId { get; set; }
        public int ProcessTypeId { get; set; }
        public Guid? OperatorId { get; set; }
        public long From{ get; set; }
        public long To { get; set; }
        public int DeliveryTypeId { get; set; }
        public int Parent { get; set; }
    }
    public class GetProcessByPageResultDto : BaseApiPageResult
    {
        public List<ProcessDto> Object { get; set; }
    }
}
