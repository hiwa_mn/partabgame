﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetProcessTypesDto 
    {
    }
    public class GetProcessTypesResultDto : BaseApiResult
    {
        public List<ProcessTypeDto> Object { get; set; }
    }

}
