﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetRoomsResultDto : BaseApiResult
    {
        public List<RoomDto> Object { get; set; }
    }
}
