﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetTransactionsByPageDto : BaseByUserPageDto
    {
        public int TransactionCategoryId { get; set; }

    }
    public class GetTransactionsByPageResultDto : BaseApiPageResult
    {
        public List<TransactionDto> Object { get; set; }
    }
}
