﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class GetWalletDto : BaseByUserDto
    {
    }
    public class GetWalletResultDto : BaseApiResult
    {
        public int Object { get; set; }
    }

}
