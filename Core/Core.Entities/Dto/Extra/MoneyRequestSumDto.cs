﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class MoneyRequestSumDto: BaseApiResult
    {
        public int Amount{ get; set; }
    }


}
