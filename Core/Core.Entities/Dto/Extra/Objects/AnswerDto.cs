﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class AnswerDto
    {
        public string Json { get; set; }
        public long CreatedAt { get; internal set; }
        public bool IsForAdmin { get; internal set; }
        public Guid? OperatorId { get; internal set; }
    }
}
