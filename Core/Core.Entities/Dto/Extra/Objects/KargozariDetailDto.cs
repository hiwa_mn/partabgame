﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class KargozariDetailDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public CityDto City { get; set; }
        public int EmployeeCount{ get; set; }
        public int DeliveryCount{ get; set; }
        public double? Lat{ get; set; }
        public double? Lon{ get; set; }
        public string Address{ get; set; }
        public string Description{ get; set; }
        public string Merchant { get; internal set; }
    }
}
