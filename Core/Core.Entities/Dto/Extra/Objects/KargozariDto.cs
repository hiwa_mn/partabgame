﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class KargozariDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public CityDto City { get; set; }
        public double? Lat { get;  set; }
        public double? Lon { get;  set; }
        public string Merchant { get;  set; }
        public string Address { get;  set; }
        public string Description { get;  set; }
    }
}
