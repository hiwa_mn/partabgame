﻿
using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class MoneyRequestDto
    {
        public Guid Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
        public int Status{ get; set; }        
        public UserShortDto User{ get; set; }
        public long CreatedAt { get; internal set; }        
        public long? ChangeStatusTime { get; internal set; }        
    }

   
}
