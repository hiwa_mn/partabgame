﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class ProcessDto
    {
        public long LastUpdateDate { get; set; }
        public FixedIntDto ProcessType { get; set; }
        public FixedIntDto ProcessStatus { get; set; }
        public string Json { get; set; }
        public UserShortDto Operator { get; set; }
        public KargozariDto Kargozari { get; set; }
        public bool IsSeen { get; set; }
        public long AssignDate { get; set; }
        public bool IsTemp { get; set; }
        public int Price { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }        
        public FixedIntDto City { get; set; }
        public FixedIntDto DeliveryType { get; set; }
        public UserShortDto Delivery { get; set; }

        public List<AnswerDto> Answers { get; set; }
        public List<ImageDto> Images { get; set; }
        public Guid Id { get; internal set; }
        public UserDto User { get; internal set; }
    }
}
