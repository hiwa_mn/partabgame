﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class RoomDto
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public int Price{ get; set; }
    }
}
