﻿using System;
using System.Collections.Generic;

namespace Core.Entities.Dto
{
    public class SendUserImagesDto:BaseByUserDto
    {        
        public List<UserImageDto> Images{ get; set; }
        public Guid? ProfileImageId { get; set; }
    }

    public class UserImageDto
    {
        public Guid ImageId { get; set; }
        public int ImageTypeId { get; set; }
    }
}
