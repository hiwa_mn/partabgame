﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Entities
{
    public class ImageStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserImages> UserImage { get; set; }

    }
}