﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.Auth;

namespace Core.Entities
{
    public class MoneyRequest : UserBaseEntity
    {               
        public int Amount { get; set; }
        public string Description{ get; set; }
        public int Status{ get; set; }
        public long? ChangeStatusTime { get; set; }
    }
}
