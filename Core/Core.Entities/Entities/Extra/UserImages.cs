﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Entities
{
    public class UserImages : UserBaseEntity
    {
        public int ImageTypeId { get; set; }
        public ImageType ImageType { get; set; }        
        public int ImageStatusId { get; set; }
        public ImageStatus ImageStatus { get; set; }
        public Guid DocumentId { get; set; }
        public Document Document { get; set; }
    }
}