﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Entities
{
    public class Slider : BaseEntity
    {                
        public string Destination { get; set; }
        public string Link{ get; set; }
        public Guid DocumentId{ get; set; }
        public Document Document{ get; set; }
    }
}