﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.Auth;

namespace Core.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public string Email{ get; set; }
        public long?  Birthday{ get; set; }
        public int?  Gender{ get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public int Status{ get; set; }

        public string Bio{ get; set; }

        public int? CityId { get; set; }
        public City City { get; set; }

        public double? Lat { get; set; }
        public double? Lon { get; set; }
        


        public Guid? ProfileImageId { get; set; }
        public Document ProfileImage { get; set; }

        public List<Device> Device{ get; set; }
        public List<Notification> Notification { get; set; }
        public List<UserRole> UserRole { get; set; }
        public List<Transaction> Transaction { get; set; }
        public List<UserImages> UserImage { get; set; }
        public string Sheba { get; set; }
        public List<MoneyRequest> MoneyRequest { get; set; }

        public void SetPassword(string password)
        {
            Encrypter encrypter = new Encrypter();

            Salt = encrypter.GetSalt();
            Password = encrypter.GetHash(password, Salt);
        }

        public bool ValidatePassword(string password, IEncrypter encrypter) =>
            Password.Equals(encrypter.GetHash(password, Salt));
    }
}
