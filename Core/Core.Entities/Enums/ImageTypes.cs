﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Enums
{
    public enum ImageTypes
    {
        [Display(Description = "شناسنامه")]
        Shenasnameh = 1,
        [Display(Description = "کارت ملی")]
        Card = 2,
        [Display(Description = "دفترچه")]
        Daftarcheh = 3,

    }
}
