﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Enums
{
    public enum MoneyRequestCode
    {
        [Display(Description = "تازه ثبت شده")]
        Pending = 1,
        [Display(Description = "تایید شده")]
        Accepted = 2,
        [Display(Description = "رد شده")]
        Rejected = 3

    }
}
