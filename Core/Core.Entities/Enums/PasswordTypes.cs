﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Enums
{
    public enum PasswordTypes
    {
        [Display(Description = "ورود")]
        Login= 1,
        [Display(Description = "فیش")]
        Fish = 2,
        [Display(Description = "سابقه")]
        Sabeghe = 3

    }
}
