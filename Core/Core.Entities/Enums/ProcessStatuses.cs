﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Enums
{
    public enum ProcessStatuses
    {
        [Display(Description = "در حال انتظار")]
        Pending = 1,
        [Display(Description = "پاسخ داده شده")]
        Answered = 2,
        [Display(Description = "مرحله اول بازیابی سابقه")]
        SabegheFirst = 3,

    }
}

