﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Enums
{
    public enum UserType
    {
        [Display(Description = "مدیر کل")]
        Admin= 1,
        [Display(Description = "مدیر کارگزاری")]
        AgencyAdmin = 2,
        [Display(Description = "کاربر کارگزاری")]
        AgencyAgent = 3,
        [Display(Description = "پیک")]
        Delivery = 4

    }
}
