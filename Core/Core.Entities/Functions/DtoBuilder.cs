﻿using Core.Entities.Dto;
using Enums;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace Core.Entities.Functions
{
    public class DtoBuilder
    {
        public static TransactionDto CreateTransactionDto(Transaction p)
        {
            return new TransactionDto
            {
                Authority = p.Authority,
                CreatedAt = p.CreatedAt,
                Description = p.Description,
                Id = p.Id,
                IsSuccessful = p.IsSuccessful,
                Price = p.Amount,
                RefId = p.RefId,
                TransactionCategory = CreateTransactionCategoryDto(p.TransactionCategory),
                User = CreateUserShortDto(p.User)
            };
        }

        private static TransactionCategoryDto CreateTransactionCategoryDto(TransactionCategory p)
        {
            return new TransactionCategoryDto
            {
                Id = p.Id,
                Name = p.Name
            };
        }

        private static UserShortDto CreateUserShortDto(User p)
        {
            return new UserShortDto
            {
                Id = p.Id,
                Name = p.Name,
                ProfileImage = p.ProfileImage != null ? CreateImageDto(p.ProfileImage) : null,
                Sheba = p.Sheba

            };
        }

        public static MoneyRequestDto CreateMoneyRequestDto(MoneyRequest p)
        {
            return new MoneyRequestDto
            {
                Amount = p.Amount,
                CreatedAt = p.CreatedAt,
                ChangeStatusTime = p.ChangeStatusTime,
                Description = p.Description,
                Id = p.Id,
                Status = p.Status,
                User = CreateUserShortDto(p.User)
            };
        }

        public static SliderDto CreateSliderDto(Slider p)
        {
            throw new NotImplementedException();
        }

        public static UserDto CreateUserDto(User p)
        {
            UserDto user = new UserDto
            {
                Birthday = p.Birthday,
                CreatedAt = p.CreatedAt,
                Email = p.Email,
                FamilyName = p.FamilyName,
                Gender = p.Gender,
                Id = p.Id,
                Sheba = p.Sheba,
                Location = p.City != null ? CreateLocationDto(p.City) : null,
                Mobile = p.Mobile,
                Name = p.Name,
                ProfileImage = p.City != null ? CreateImageDto(p.ProfileImage) : null,
                Status = p.Status
            };
            if (user.Location != null)
            {
                user.Location.Lat = p.Lat;
                user.Location.Lon = p.Lon;
            }
            return user;
        }

        public static RoomDto CreateRoomDto(Room p)
        {
            return new RoomDto
            {
                Id = p.Id,
                Name = p.Name,
                Price = p.Price
            };
        }

        private static ImageDto CreateImageDto(Document p)
        {
            return new ImageDto
            {
                Id = p.Id,
                Address = p.Location
            };
        }

        private static LocationDto CreateLocationDto(City p)
        {
            return new LocationDto
            {
                CityId = p.Id,
                CityName = p.Name,
                CountryId = p.Province?.Country?.Id,
                CountryName = p.Province?.Country?.Name,
                ProvienceId = p.ProvinceId,
                ProvienceName = p.Province?.Name
            };
        }
    }
}
