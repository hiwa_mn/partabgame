﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class ImageStatus : IEntityTypeConfiguration<Core.Entities.ImageStatus>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.ImageStatus> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.Property(p => p.Id).ValueGeneratedNever();
        }
    }
}