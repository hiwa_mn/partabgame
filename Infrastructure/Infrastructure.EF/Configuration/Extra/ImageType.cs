﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class ImageType : IEntityTypeConfiguration<Core.Entities.ImageType>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.ImageType> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.Property(p => p.Id).ValueGeneratedNever();
        }
    }
}