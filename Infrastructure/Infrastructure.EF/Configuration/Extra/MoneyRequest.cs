﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class MoneyRequest : IEntityTypeConfiguration<Core.Entities.MoneyRequest>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.MoneyRequest> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasOne(p => p.User).WithMany(p => p.MoneyRequest).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);            

        }
    }
}