﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class Room : IEntityTypeConfiguration<Core.Entities.Room>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.Room> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.Property(p => p.Id).ValueGeneratedNever();
        }
    }
}