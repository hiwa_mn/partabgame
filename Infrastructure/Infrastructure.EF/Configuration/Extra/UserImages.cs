﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EF.Configuration
{
    public class UserImages : IEntityTypeConfiguration<Core.Entities.UserImages>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.UserImages> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasOne(p => p.User).WithMany(p => p.UserImage).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.Document).WithMany(p => p.UserImage).HasForeignKey(p => p.DocumentId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.ImageType).WithMany(p => p.UserImage).HasForeignKey(p => p.ImageTypeId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.ImageStatus).WithMany(p => p.UserImage).HasForeignKey(p => p.ImageStatusId).OnDelete(DeleteBehavior.Restrict);

        }
    }
}