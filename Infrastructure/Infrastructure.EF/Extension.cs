﻿using Core.ApplicationServices;
using Core.Contracts;
using Core.Entities;
using Core.Entities.GlobalSettings;
using Domain.Contract;
using Infrastructure.EF.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.Notification;
using Utility.Tools.SMS.Rahyab;
using Utility.Tools.Swager;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace Infrastructure.EF
{
    public static class Extension
    {
        


        public static void AddApplicationServices(this IServiceCollection services)
        {
            var applicationServiceType = typeof(IApplicationService).Assembly;
            var AllApplicationServices = applicationServiceType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.FullName.Contains("ApplicationService")).ToList();
            foreach (var type in AllApplicationServices)
            {
                //Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            var repositpryType = typeof(Repository<>).Assembly;
            var AllRepositories = repositpryType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.Name.Contains("Repository") && !x.Name.StartsWith("Repository")).ToList();
            foreach (var type in AllRepositories)
            {
                //Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }

        public static void ConfigureServices(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            configuration.GetSection<AdminSettings>();
            services.AddApplicationServices();
            services.AddRepositories();
            services.AddSwager();
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("CorsPolicy",
            //        builder => builder.AllowAnyOrigin()
            //        .AllowAnyMethod()
            //        .AllowAnyHeader()
            //        .AllowCredentials());
            //});
            //services.AddMvc(options =>
            //{
            //    //options.Filters.Add(typeof(LogFilterAttribute));
            //    options.Filters.Add(typeof(CustomExceptionFilter));
            //    //options.ModelBinderProviders.Insert(0, new CustomBinderProvider());
            //}).AddSessionStateTempDataProvider().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddJwt(configuration);
            //configuration.GetSection<RahyabParameters>();
            services.AddScoped<IContext, MainContext>();
            //services.AddDbContext<MainContext>(o => o.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("Infrastructure.EndPoint")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IEncrypter, Encrypter>();
            services.AddScoped<INotification, RahyabService>();

            //configuration.GetSection<RahyabParameters>();
            //services.AddJwt(configuration);
            services.AddScoped<IContext, MainContext>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IEncrypter, Encrypter>();

            services.AddScoped<INotification, TrezService>();
            services.AddScoped<IFireBase, FireBase>();

        }


        public static void AddEntityConfiguration(this ModelBuilder builder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                         .Where(t => t.GetInterfaces().Any(gi => gi.IsGenericType && gi.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))).ToList();

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                builder.ApplyConfiguration(configurationInstance);
            }
        }

        public static void SeedData(this ModelBuilder builder)
        {

            

            builder.Entity<Role>().HasData(new Role { Id = 1, Name = "مدیر کل" });
            builder.Entity<Role>().HasData(new Role { Id = 2, Name = "مدیر کارگزاری" });
            builder.Entity<Role>().HasData(new Role { Id = 3, Name = "کاربر کارگزاری" });
            builder.Entity<Role>().HasData(new Role { Id = 4, Name = "پیک" });


            builder.Entity<Country>().HasData(new Country { Id = 1, Name = "Iran" });
            builder.Entity<Province>().HasData(new Province { Id = 1, Name = "Isfahan", CountryId = 1 });
            builder.Entity<City>().HasData(new City { Id = 1, Name = "Sede", ProvinceId = 1 });
            builder.Entity<City>().HasData(new City { Id = 2, Name = "Shahin Shahr", ProvinceId = 1 });
            builder.Entity<City>().HasData(new City { Id = 3, Name = "Jalal Abad", ProvinceId = 1 });
            builder.Entity<City>().HasData(new City { Id = 4, Name = "Ghahdrijan", ProvinceId = 1 });
            
          
            
            
            builder.Entity<TransactionCategory>().HasData(new TransactionCategory { Id = 1, Name = "ورود به روم"});
            builder.Entity<TransactionCategory>().HasData(new TransactionCategory { Id = 2, Name = "برد"});
            builder.Entity<TransactionCategory>().HasData(new TransactionCategory { Id = 3, Name = "باخت"});
            builder.Entity<TransactionCategory>().HasData(new TransactionCategory { Id = 4, Name = "واریز از درگاه"});
            builder.Entity<TransactionCategory>().HasData(new TransactionCategory { Id = 5, Name = "تسویه"});
            
            



            
            
            builder.Entity<ImageStatus>().HasData(new ImageStatus { Id = 1, Name = "در حال انتظار"});
            builder.Entity<ImageStatus>().HasData(new ImageStatus { Id = 2, Name = "تایید شده"});
            builder.Entity<ImageStatus>().HasData(new ImageStatus { Id = 3, Name = "رد شده"});
            
            builder.Entity<ImageType>().HasData(new ImageType { Id = 1, Name = "شناسنامه"});
            builder.Entity<ImageType>().HasData(new ImageType { Id = 2, Name = "کارت ملی"});
            builder.Entity<ImageType>().HasData(new ImageType { Id = 3, Name = "دفترچه"});
            
            
            builder.Entity<Room>().HasData(new Room { Id = 1, Name = "اتاق 1" , Price = 5000});
            builder.Entity<Room>().HasData(new Room { Id = 2, Name = "اتاق 2" , Price = 10000});
            builder.Entity<Room>().HasData(new Room { Id = 3, Name = "اتاق 3" , Price = 20000});
            builder.Entity<Room>().HasData(new Room { Id = 4, Name = "اتاق 4" , Price = 50000});
            builder.Entity<Room>().HasData(new Room { Id = 5, Name = "اتاق 5" , Price = 100000});
            
            
            
            

            var now = DateTime.Now.ToUnix();


            User user = new User 
            { 
                Mobile = "09179887250",
                Name = "مدیر",
                FamilyName = "کل",
                Id = Guid.Parse("8114eb2e-3588-4239-98a4-f7e3023674e8"),
                Status = Enums.EntityStates.Active.ToInt(), 
               
            };

            user.SetPassword("hani1052");
            builder.Entity<User>().HasData(user);
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now,Id= Guid.Parse("44fc4a46-6bd7-45d0-a911-b989ed4a944c") ,RoleId=2,UserId = Guid.Parse("8114eb2e-3588-4239-98a4-f7e3023674e8") });
            //using (var str = new StreamReader(@"Province.json"))
            //{
            //    var resul = Api.ToObject<List<ProvinceAndCitys>>(str.ReadToEnd());

            //    int provienceCounter = 1, cityCounter = 1;


            //    resul.ForEach(p =>
            //    {
            //        List<City> cities = new List<City>();
            //        p.cities.ForEach(q =>
            //        {
            //            cities.Add(new Core.Entities.City { Name = q.name, ProvinceId = provienceCounter, Id = cityCounter++ });
            //        });
            //        builder.Entity<Province>().HasData(new Core.Entities.Province { Id = provienceCounter++, Name = p.name });
            //        builder.Entity<City>().HasData(cities.ToArray());
            //    });
            //}



        }
    }
}
