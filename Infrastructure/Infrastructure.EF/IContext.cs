﻿
using Core.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EF
{
    public interface IContext 
    {
        DbSet<City> Cities { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Province> Provinces { get; set; }
        DbSet<Document> Documents { get; set; }
        DbSet<ActiveCode> ActiveCodes { get; set; }
        DbSet<Device> Devices { get; set; }
        DbSet<Notification> Notifications { get; set; }
        DbSet<Setting> Settings { get; set; }
        DbSet<Update> Updates { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<Slider> Sliders{ get; set; }
        DbSet<ImageStatus> ImageStatus { get; set; }
        DbSet<ImageType> ImageType { get; set; }
        DbSet<Transaction> Transaction { get; set; }
        DbSet<TransactionCategory> TransactionCategory { get; set; }
        DbSet<UserImages> UserImages { get; set; }
        DbSet<Room> Rooms { get; set; }
        DbSet<MoneyRequest> MoneyRequests { get; set; }
        

        void SaveChanges();
        

    }
}