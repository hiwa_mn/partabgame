﻿using Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Reflection;

namespace Infrastructure.EF
{
    public class MainContext : DbContext, IContext
    {


        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<ActiveCode> ActiveCodes { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Update> Updates { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Slider> Sliders { get; set; }
        public DbSet<ImageStatus> ImageStatus { get; set; }
        public DbSet<ImageType> ImageType { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionCategory> TransactionCategory { get; set; }
        public DbSet<UserImages> UserImages { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<MoneyRequest> MoneyRequests { get; set; }


        public MainContext(DbContextOptions<MainContext> options) : base(options)
        {

        }

        public MainContext()
        {
            //Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("Infrastructure.EF")/*, sqlServerOptions => sqlServerOptions.CommandTimeout(600)*/);
            //this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.AddEntityConfiguration();
            builder.SeedData();
        }

        void IContext.SaveChanges()
        {
            this.SaveChanges();
        }
    }
}
