﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Domain.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Infrastructure.EF.Services
{
    public class ProvinceRepository : Repository<Province>, IProvinceRepository
    {
        private readonly IContext ctx;

        public ProvinceRepository(IContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }

        public List<ProvinceDto> GetIncludeCties()
        {
            return ctx.Provinces.Include(p => p.City).ThenInclude(p => p.User)
                .ToList().Select(p =>
            new ProvinceDto
            {
                Id = p.Id,
                Name = p.Name,
                UserCount = p.City.SelectMany(q => q.User).Count(),
                Cities = p.City.Select(q => new CityResponse { City = new FixedIntDto { Id = q.Id, Name = q.Name }, UserCount = q.User.Count() }).ToList()
            }).ToList();
        }

        public Province GetIncludeCtiesById(GetCitiesDto dto)
        {
            return ctx.Provinces.Where(p => p.Id == dto.ProvienceId).Include(p => p.City).ThenInclude(p => p.User)
                .FirstOrDefault();
        }
    }
}
