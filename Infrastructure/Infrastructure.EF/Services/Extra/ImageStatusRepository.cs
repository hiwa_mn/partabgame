﻿using Core.Contracts;
using Core.Entities;
using Domain.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Infrastructure.EF.Services
{
    public class ImageStatusRepository : Repository<ImageStatus>, IImageStatusRepository
    {
        private readonly IContext ctx;

        public ImageStatusRepository(IContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }


    }
}
