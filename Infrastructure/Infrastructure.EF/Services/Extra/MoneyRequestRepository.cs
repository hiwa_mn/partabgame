﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using Core.Entities.GlobalSettings;
using Domain.Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Utility.Tools.General;

namespace Infrastructure.EF.Services
{
    public class MoneyRequestRepository : Repository<MoneyRequest>, IMoneyRequestRepository
    {
        private readonly IContext ctx;

        public MoneyRequestRepository(IContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }

        public GetMoneyRequestsByPageResultDto GetMoneyRequestsByPage(GetMoneyRequestsByPageDto dto)
        {
            List<MoneyRequest> mr = ctx.MoneyRequests.Where(p =>
            (dto.UserId == null || dto.UserId == p.UserId) &&
            (dto.Status == 0 || dto.Status == p.Status)
            ).
            Include(p => p.User).ThenInclude(p => p.ProfileImage).
                ToList();
            var res = mr.OrderByDescending(p => p.CreatedAt).Skip((dto.PageNo - 1) * AdminSettings.Block).
                Take(AdminSettings.Block).Select(p => DtoBuilder.CreateMoneyRequestDto(p)).ToList();
            return new GetMoneyRequestsByPageResultDto
            {
                Object = res,
                Status = true,
                Page = new PageDto
                {
                    PageNo = dto.PageNo,
                    CurrentCount = res.Count,
                    Total = mr.Count
                }
            };
        }

        public MoneyRequestSumDto GetMonyRequestSum(GetMoneyRequestSumDto dto)
        {

           var startDate = dto.CreateAt.ToDate().Date.ToUnix();
           var endDate = dto.CreateAt.ToDate().Date.AddTicks(-1).AddDays(1).ToUnix();

            var moneySum = ctx.MoneyRequests.Where(p => ( startDate <= dto.CreateAt && dto.CreateAt >= endDate) &&
                                                        (p.Status == 0 || p.Status == dto.Status))
                                                        .Select(p => p.Amount).Sum();

            return new MoneyRequestSumDto
            {
                Amount = moneySum,
                Status = true,
                Message = "عملیات با موفقیت انجام شد"
                
            };
        }
    }
}
