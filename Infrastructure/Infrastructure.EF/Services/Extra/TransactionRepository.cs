﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using Core.Entities.GlobalSettings;
using Domain.Contract;
using Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Infrastructure.EF.Services
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        private readonly IContext ctx;

        public TransactionRepository(IContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }

        public Transaction GetByAuthority(string authority)
        {
            return ctx.Transaction.Where(p => p.Authority == authority).ToList().FirstOrDefault();
        }

        public List<TransactionDto> GetTransactionsByPage(GetTransactionsByPageDto dto)
        {
            return ctx.Transaction.
                Include(p => p.User).Include(p => p.TransactionCategory).
                Where(p =>
                    (p.UserId == dto.UserId || dto.UserId == null) &&
                    (p.TransactionCategoryId == dto.TransactionCategoryId || dto.TransactionCategoryId == 0)
                ).
                OrderByDescending(p=>p.CreatedAt).
                Skip((dto.PageNo-1) * AdminSettings.Block).
                Take(AdminSettings.Block).
                Include(p=>p.User).ThenInclude(p=>p.ProfileImage)
                .ToList().
                Select(p => DtoBuilder.CreateTransactionDto(p)).ToList();
        }

        public int GetTransactionsByPageCount(GetTransactionsByPageDto dto)
        {
            return ctx.Transaction.
                Include(p => p.User).
                 Where(p =>
                    (p.UserId == dto.UserId || dto.UserId == null) &&
                    (p.TransactionCategoryId == dto.TransactionCategoryId || dto.TransactionCategoryId == 0)
                ).ToList().Count();
        }

        public int GetWallet(BaseByUserDto dto)
        {
            var trans = ctx.Transaction.Where(p => p.UserId == dto.UserId && p.IsSuccessful).ToList().Sum(p => p.Amount);
            var req = ctx.MoneyRequests.Where(p => p.UserId == dto.UserId && p.Status == MoneyRequestCode.Pending.ToInt()).ToList().Sum(p => p.Amount);
            return trans - req;
        }

        public bool IsAuthorityExist(string authority)
        {
            return ctx.Transaction.Any(p => p.Authority == authority);
        }
    }
}
