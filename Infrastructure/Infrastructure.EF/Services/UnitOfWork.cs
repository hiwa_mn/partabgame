﻿using Core.Contracts;
using Infrastructure.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Contract
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(
            IContext ctx,
            ICityRepository City,
        ICountryRepository Country,
        IProvinceRepository Province,
        IDocumentRepository Document,
        IActiveCodeRepository ActiveCode,
        IDeviceRepository Device,
        INotificationRepository Notification,
        ISettingRepository Setting,
        IUpdateRepository Update,
        IRoleRepository Roles,
        IUserRepository Users,
        IUserRoleRepository UserRoles,
        ISliderRepository Slider,
        IImageStatusRepository ImageStatus,
        IImageTypeRepository ImageType,
        ITransactionCategoryRepository TransactionCategory,
        ITransactionRepository Transaction,
        IUserImagesRepository UserImages,
        IRoomRepository Rooms,
        IMoneyRequestRepository MoneyRequests
            )
        {
            this.ctx = ctx;
            this.City = City;
            this.Country = Country;
            this.Province = Province;
            this.Document = Document;
            this.ActiveCode = ActiveCode;
            this.Device = Device;
            this.Notification = Notification;
            this.Setting = Setting;
            this.Update = Update;
            this.Roles = Roles;
            this.Users = Users;
            this.UserRoles = UserRoles;
            this.Slider = Slider;
            this.ImageStatus = ImageStatus;
            this.ImageType = ImageType;
            this.TransactionCategory = TransactionCategory;
            this.Transaction = Transaction;
            this.UserImages = UserImages;
            this.Rooms = Rooms;
            this.MoneyRequests = MoneyRequests;
        }
        public IContext ctx { get; set; }
        public ICityRepository City { get; set; }
        public ICountryRepository Country { get; set; }
        public IProvinceRepository Province { get; set; }
        public IDocumentRepository Document { get; set; }
        public IActiveCodeRepository ActiveCode { get; set; }
        public IDeviceRepository Device { get; set; }
        public INotificationRepository Notification { get; set; }
        public ISettingRepository Setting { get; set; }
        public IUpdateRepository Update { get; set; }
        public IRoleRepository Roles { get; set; }
        public IUserRepository Users { get; set; }
        public IUserRoleRepository UserRoles { get; set; }
        public ISliderRepository Slider { get; set; }
        public IImageStatusRepository ImageStatus { get; set; }
        public IImageTypeRepository ImageType { get; set; }
        public ITransactionCategoryRepository TransactionCategory { get; set; }
        public ITransactionRepository Transaction { get; set; }
        public IUserImagesRepository UserImages { get; set; }
        public IRoomRepository Rooms { get; set; }
        public IMoneyRequestRepository MoneyRequests { get; set; }


        public void Complete()
        {
            ctx.SaveChanges();
        }
    }
}
