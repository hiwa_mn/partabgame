﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Dto;
using Core.Entities.Functions;
using Core.Entities.GlobalSettings;
using Domain.Contract;
using Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Utility;

namespace Infrastructure.EF.Services
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IContext ctx;

        public UserRepository(IContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }

        public User GetByMobile(string mobile)
        {
            return ctx.Users.Where(p => p.Mobile == mobile).
                Include(p => p.City).
                Include(p => p.UserRole).ThenInclude(p => p.Role).
                Include(p => p.UserImage).ThenInclude(p => p.Document).
                Include(p => p.ProfileImage).
                Include(p=>p.Transaction).
                ToList().FirstOrDefault();
        }
        public User GetByEmail(string Email)
        {
            return ctx.Users.Where(p => p.Email == Email).
                Include(p => p.City).ThenInclude(p=>p.Province).ThenInclude(p=>p.Country).
                Include(p => p.ProfileImage).
                Include(p=>p.UserRole).ThenInclude(p=>p.Role).
                FirstOrDefault();
        }

        public User GetByEmailIncludingRoles(string email)
        {
            return ctx.Users.Where(p => p.Email == email).
                Include(p => p.City).Include(p => p.ProfileImage).Include(p => p.UserRole).FirstOrDefault();
        }
        public User GetByMobileIncludingRoles(string mobile)
        {
            return ctx.Users.
                Where(p => p.Mobile == mobile).
                Include(p => p.City).
                Include(p => p.ProfileImage).
                Include(p => p.UserRole).ThenInclude(p => p.Role).
                FirstOrDefault();
        }



        public List<UserDto> GetUsersByPage(GetUsersByPageDto dto)
        {
            var result = ctx.Users.Include(p => p.UserRole).ThenInclude(p => p.Role).Where(p =>
                ((dto.RoleId == -1 && p.UserRole != null && p.UserRole.Count != 0) || (dto.RoleId == 0 && (p.UserRole == null || p.UserRole.Count == 0)) || (dto.RoleId != 0 && p.UserRole.Any(q => q.RoleId == dto.RoleId)))
            ).OrderByDescending(p => p.CreatedAt).
                Skip((dto.PageNo - 1) * AdminSettings.Block).
                Take(AdminSettings.Block).
                Include(p => p.City).ThenInclude(p => p.Province).
                Include(p => p.ProfileImage).
                Select(p => DtoBuilder.CreateUserDto(p)).
                ToList();
            return result;                                
        }

        public bool IsExist(string Email)
        {
            return ctx.Users.Any(p => p.Email == Email);
        }

        public int GetUsersByPageCount(GetUsersByPageDto dto)
        {
            return ctx.Users.Include(p => p.UserRole).ThenInclude(p => p.Role).Where(p =>
                ((dto.RoleId == -1 && p.UserRole != null && p.UserRole.Count != 0) || (dto.RoleId == 0 && (p.UserRole == null || p.UserRole.Count == 0)) || (dto.RoleId != 0 && p.UserRole.Any(q => q.RoleId == dto.RoleId)))
            ).Count();
        }

        public User GetById(Guid userId)
        {
            return ctx.Users.
                Where(p => p.Id == userId).
                Include(p => p.UserRole).ThenInclude(p => p.Role).
                Include(p => p.City).
                Include(p => p.ProfileImage).
                Include(p => p.UserImage).ThenInclude(p => p.Document).
                Include(p => p.Transaction).
                FirstOrDefault();
        }

        

        public List<User> GetAllByDetails()
        {
            return ctx.Users.Include(p => p.UserRole).ToList();
        }
    }
}
