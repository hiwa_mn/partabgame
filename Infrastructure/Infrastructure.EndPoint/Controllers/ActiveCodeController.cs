﻿using Core.ApplicationServices;
using Core.Entities.Dto;
using Infrastructure.EndPoint.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.MobileEndPoint.Controllers
{
    public class ActiveCodeController : SimpleController
    {
        private readonly ISendActiveCode sendActiveCode;
        private readonly ICheckActiveCode checkActiveCode;
        private readonly ILogin login;

        public ActiveCodeController(
            ISendActiveCode sendActiveCode,
            ICheckActiveCode checkActiveCode,
            ILogin login
            )
        {
            this.sendActiveCode = sendActiveCode;
            this.checkActiveCode = checkActiveCode;
            this.login = login;
        }
        [HttpPost]
        public ActionResult<BaseApiResult> SendActiveCode([FromBody] SendActiveCodeDto dto)
        {
            return sendActiveCode.Execute(dto);
        }
        [HttpPost]
        public ActionResult<LoginResultDto> Login([FromBody] LoginDto dto)
        {
            return login.Execute(dto);
        }
        [HttpPost]
        public ActionResult<CheckActiveCodeResultDto> CheckActiveCode([FromBody] CheckActiveCodeDto dto)
        {
            return checkActiveCode.Execute(dto);
        }
    }
}
