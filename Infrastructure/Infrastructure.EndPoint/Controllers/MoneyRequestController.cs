﻿using Core.ApplicationServices;
using Core.Entities.Dto;
using Infrastructure.EndPoint.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.MobileEndPoint.Controllers
{
    public class MoneyRequestController : AuthorizedController
    {
        private readonly IAddMoneyRequest addMoneyRequest;
        private readonly IGetMoneyRequestsByPage getMoneyRequestsByPage;
        private readonly IChangeMoneyResuestStatus changeMoneyResuestStatus;
        private readonly IGetMoneyRequetsSum getMoneyRequetsSum;

        public MoneyRequestController(
            IAddMoneyRequest addMoneyRequest,
            IGetMoneyRequestsByPage getMoneyRequestsByPage,
            IChangeMoneyResuestStatus changeMoneyResuestStatus,
            IGetMoneyRequetsSum getMoneyRequetsSum
            )
        {
            this.addMoneyRequest = addMoneyRequest;
            this.getMoneyRequestsByPage = getMoneyRequestsByPage;
            this.changeMoneyResuestStatus = changeMoneyResuestStatus;
            this.getMoneyRequetsSum = getMoneyRequetsSum;
        }
        [HttpPost]
        public ActionResult<BaseApiResult> AddMoneyRequest([FromBody] AddMoneyRequestDto dto)
        {
            return addMoneyRequest.Execute(dto);
        }
        [HttpGet]
        public ActionResult<GetMoneyRequestsByPageResultDto> GetMoneyRequestsByPage([FromQuery] GetMoneyRequestsByPageDto dto)
        {
            return getMoneyRequestsByPage.Execute(dto);
        }

        [HttpGet]
        public ActionResult<MoneyRequestSumDto> GetMoneyRequestsSum([FromQuery]GetMoneyRequestSumDto dto)
        {
            return getMoneyRequetsSum.Execute(dto);
        }

        [HttpPost]
        public ActionResult<BaseApiResult> ChangeMoneyResuestStatus([FromBody] ChangeMoneyResuestStatusDto dto)
        {
            return changeMoneyResuestStatus.Execute(dto);
        }
        
    }
}
