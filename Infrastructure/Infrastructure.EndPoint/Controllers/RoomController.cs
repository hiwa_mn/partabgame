﻿using Core.ApplicationServices;
using Core.Entities.Dto;
using Infrastructure.EndPoint.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.MobileEndPoint.Controllers
{
    public class RoomController : AuthorizedController
    {
        private readonly IGetRooms getRooms;

        public RoomController(
            IGetRooms getRooms
            )
        {            
            this.getRooms = getRooms;
        }
        [HttpGet]
        public ActionResult<GetRoomsResultDto> GetRooms()
        {
            return getRooms.Execute();
        }
        
    }
}
