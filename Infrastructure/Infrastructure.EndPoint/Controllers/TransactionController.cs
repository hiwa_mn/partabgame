﻿using Core.ApplicationServices;
using Core.Entities.Dto;
using Infrastructure.EndPoint.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.MobileEndPoint.Controllers
{
    public class TransactionController : SimpleController
    {
        private readonly IGetTransactionsByPage getTransactionsByPage;
        private readonly IAddTransaction addTransaction;
        private readonly IGetWallet getWallet;

        public TransactionController(
            IGetTransactionsByPage getTransactionsByPage,
            IAddTransaction addTransaction,
            IGetWallet getWallet
            )
        {
            this.getTransactionsByPage = getTransactionsByPage;
            this.addTransaction = addTransaction;
            this.getWallet = getWallet;
        }
        [HttpGet]
        public ActionResult<GetTransactionsByPageResultDto> GetTransactionsByPage([FromQuery] GetTransactionsByPageDto dto)
        {
            return getTransactionsByPage.Execute(dto);
        }
        [HttpGet]
        public ActionResult<GetWalletResultDto> GetWalletBalance([FromQuery] GetWalletDto dto)
        {
            return getWallet.Execute(dto);
        }
        [HttpPost]
        public ActionResult<BaseApiResult> AddTransaction([FromBody] AddTransactionDto dto)
        {
            return addTransaction.Execute(dto);
        }

    }
}
