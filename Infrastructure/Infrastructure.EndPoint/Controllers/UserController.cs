﻿using Core.ApplicationServices;
using Core.Entities.Dto;
using Infrastructure.EndPoint.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.MobileEndPoint.Controllers
{
    public class UserController : AuthorizedController
    {
        private readonly IEditProfile editProfile;

        public UserController(
            IEditProfile editProfile
            )
        {
            this.editProfile = editProfile;
        }
        [HttpPost]
        public ActionResult<BaseApiResult> EditProfile([FromBody] EditProfileDto dto)
        {
            return editProfile.Execute(dto);
        }
        
    }
}
