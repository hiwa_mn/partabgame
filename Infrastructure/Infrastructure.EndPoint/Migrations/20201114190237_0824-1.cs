﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.MobileEndPoint.Migrations
{
    public partial class _08241 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ChangeStatusTime",
                table: "MoneyRequests",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: new Guid("44fc4a46-6bd7-45d0-a911-b989ed4a944c"),
                column: "CreatedAt",
                value: 1605380556L);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8114eb2e-3588-4239-98a4-f7e3023674e8"),
                columns: new[] { "Password", "Salt" },
                values: new object[] { "nOUnr5INbZjWolJvquUQNXV2pd7/UJU9GnOX3dKbXRCz0pH01pyzWQ==", "gDmdyls8b5166iWjpPr/Qj2u0rAFBoQbFMCyzPwpfbM631Db/7xkdQ==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangeStatusTime",
                table: "MoneyRequests");

            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: new Guid("44fc4a46-6bd7-45d0-a911-b989ed4a944c"),
                column: "CreatedAt",
                value: 1604922818L);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8114eb2e-3588-4239-98a4-f7e3023674e8"),
                columns: new[] { "Password", "Salt" },
                values: new object[] { "fO4Sdmlse5x19jp8JZLYOJwfaHABcgQIrXP+gtsSxDGueOqylCCA2A==", "3c8Igif1pIt+gdrKcQdyGHQhgSPv9JNYD1aYgjkNbiMbLHOLqOwYlw==" });
        }
    }
}
