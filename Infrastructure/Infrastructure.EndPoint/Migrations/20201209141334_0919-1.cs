﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.MobileEndPoint.Migrations
{
    public partial class _09191 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: new Guid("44fc4a46-6bd7-45d0-a911-b989ed4a944c"),
                column: "CreatedAt",
                value: 1607523214L);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8114eb2e-3588-4239-98a4-f7e3023674e8"),
                columns: new[] { "Password", "Salt" },
                values: new object[] { "C0HZh8a0sBBWPn2BSiiIQ2YfuTFW4NMm9TcHcgBGYFGQ57x8ET5uEQ==", "N3Z/hbWjqWbDTpB0/jJBx9iaFKoySYIZZMh0/8JQkPUg6WGkdngbQw==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: new Guid("44fc4a46-6bd7-45d0-a911-b989ed4a944c"),
                column: "CreatedAt",
                value: 1605380556L);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8114eb2e-3588-4239-98a4-f7e3023674e8"),
                columns: new[] { "Password", "Salt" },
                values: new object[] { "nOUnr5INbZjWolJvquUQNXV2pd7/UJU9GnOX3dKbXRCz0pH01pyzWQ==", "gDmdyls8b5166iWjpPr/Qj2u0rAFBoQbFMCyzPwpfbM631Db/7xkdQ==" });
        }
    }
}
