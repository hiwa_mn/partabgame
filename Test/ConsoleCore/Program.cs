﻿using Domain.Contract;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Core.Contracts;
using Utility.Tools.Auth;
using Infrastructure.EF;
using Utility.Tools.Notification;
using Core.ApplicationServices;
using Utility.Tools.SMS.Rahyab;
using Core.Entities.Dto;
using Utility.Tools;
using System.Net.Sockets;
using System.Net.WebSockets;
using Utility.Tools.General;

namespace ConsoleCore
{
    class Program
    {
        public static IUnitOfWork Iunit { get; private set; }
        public static INotification Notification { get; private set; }
        public static IEncrypter Encrypter { get; private set; }
        public static IJwtHandler JwtHandler { get; private set; }
        public static IFireBase Firebase { get; private set; }
        public static IConfiguration Configuration { get; set; }



        static async Task Main()
        {

            Config();

            //await Notification.SendAsync("salam", "09378182334");

            GetTransactionsByPage getTransactionsByPage = new GetTransactionsByPage(Iunit);

            getTransactionsByPage.Execute(new GetTransactionsByPageDto
            {
                PageNo = 1,
                UserId = "ba4a312f-2112-4376-bd04-08d88616fb2e".ToGuid(),


            });
            //Console.WriteLine(Agent.Now.ToString());
            


            GetMoneyRequetsSum getMoneyRequets = new GetMoneyRequetsSum(Iunit);

            getMoneyRequets.Execute(new GetMoneyRequestSumDto
            {

                CreateAt =1605387293,
                Status=0
            }
             );




            
            Console.ReadKey();

        }



        public static void Config()
        {
            ServiceCollection service = new ServiceCollection();
            var builder = new ConfigurationBuilder().SetBasePath(@"M:\Projects\PartabGame\Infrastructure\Infrastructure.EndPoint").AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).AddEnvironmentVariables();
            Configuration = builder.Build();

            service.AddSingleton<IConfiguration>(builder.Build());


            service.ConfigureServices(Configuration);
            service.AddDbContext<MainContext>(o => o.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            var provider = service.BuildServiceProvider();
            Iunit = provider.GetService<IUnitOfWork>();
            Encrypter = provider.GetService<IEncrypter>();
            Notification = provider.GetService<INotification>();
            JwtHandler = provider.GetService<IJwtHandler>();
            Firebase = provider.GetService<IFireBase>();
        }


    }






}
