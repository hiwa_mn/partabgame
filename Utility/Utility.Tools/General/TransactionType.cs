﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Enums
{
    public enum TransactionType
    {
        [Display(Description = "ورود به روم")]
        Pay= 1,
        [Display(Description = "برد")]
        Receive = 2,
        [Display(Description = "باخت")]
        Send= 3,
        [Display(Description = "واریز از درگاه")]
        Gateway = 4,
        [Display(Description = "تسویه")]
        Released = 5

    }
}
